import UIKit

class Coordinator {
    private let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let viewController = FormMoneyFactory.createFormMoneyViewController()
        self.navigationController.pushViewController(viewController, animated: true)
        viewController.delegate = self
        self.navigationController.isNavigationBarHidden = true
    }
}
extension Coordinator: FormMoneyViewControllerDelegate {
    func showDetailWith(information: CorrectInformation) {
        let viewController = DetailFactory.makeDetailViewController(information: information)
        viewController.delegate = self
        self.navigationController.pushViewController(viewController, animated: true)
    }
}

extension Coordinator: DetailViewControllerDelegateType {
    func goToSimulateAgain() {
        self.navigationController.popViewController(animated: true)
    }
}
