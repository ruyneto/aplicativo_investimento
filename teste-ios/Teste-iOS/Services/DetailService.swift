import UIKit
class DetailService {
}

extension DetailService: DetailServiceType {
    func getSimulation(
        correctInformation: CorrectInformation,
        callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void)) {
        let abcd = "https://api.mocki.io/v1/2c6dc5e0"
        guard let url = URL(string: abcd) else {
            callBack(.error("Erro de URL."))
            return
        }
        URLSession.shared.dataTask(with: url) {  data, _, error in
            guard error == nil, let json = data else {
                DispatchQueue.main.async {
                    callBack(.error("Error não definido."))
                }
                return
            }
            do {
                let product = try JSONDecoder().decode(DetailResponse.self, from: json )
                DispatchQueue.main.async {
                    callBack(.success(product))
                }
            } catch {
                print(error)
                DispatchQueue.main.async {
                    callBack(.error("Erro ao converter a resposta."))
                }
            }
        }.resume()
        //callBack(.error("Error Genérico 12345654"))
    }
}
