enum ResponsePattern<T> {
    case success(T)
    case error(String)
}
