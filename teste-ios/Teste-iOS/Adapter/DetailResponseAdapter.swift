import Foundation

class DetailResponseAdapter {
    static func adapt(response: DetailResponse) -> DetailViewModel {
        return DetailViewModel(
            result: response.netAmount.formatToMoney(),
            appliedValue: response.investmentParameter.investedAmount.formatToMoney(),
            bruteValue: response.grossAmount.formatToMoney(),
            rendimentValue: response.netAmountProfit.formatToMoney(),
            taxes: "\(response.taxesAmount.formatFloatNumber())".formatToMoney(),
            liquidValue: response.netAmountProfit.formatToMoney(),
            rescueDay: "\(response.investmentParameter.maturityDate)".formatToBrazilian(),
            daysPassed: "\(response.investmentParameter.maturityTotalDays)",
            monthlyRendiment: response.monthlyGrossRateProfit.formatToMoney(),
            cdiPercentual: "\(Int(response.investmentParameter.rate))%",
            rentabilityYear: response.investmentParameter.yearlyInterestRate.formatPercentageResponse(),
            rentabilityPeriod: response.rateProfit.formatPercentageResponse()
        )
    }
}
