import Foundation

struct CorrectInformation {
    var value: Decimal
    var date: Date
    var percentage: Int
}
