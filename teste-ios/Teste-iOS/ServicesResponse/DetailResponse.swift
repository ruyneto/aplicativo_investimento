import Foundation
struct DetailResponse: Codable {
    struct InvestmentParameter: Codable {
        var investedAmount: Double                      // O valor a ser investido
        var yearlyInterestRate: Decimal               // Rentabilidade anual
        var maturityTotalDays: Int                   // Dias corridos
        var maturityBusinessDays: Int                  // Dias úteis
        var maturityDate: String               // Data de vencimento
        var rate: Double                                 // Percentual do papel
        var isTaxFree: Bool
    }
    var investmentParameter: InvestmentParameter
    var grossAmount: Double                            // Valor bruto do investimento
    var taxesAmount: Double                            // Valor do IR
    var netAmount: Double                             // Valor líquido
    var grossAmountProfit: Double                      // Rentabilidade bruta
    var netAmountProfit: Double                        // Rentabilidade líquida
    var annualGrossRateProfit: Double                     // Rentabilidade bruta anual
    var monthlyGrossRateProfit: Double                     // Rentabilidade bruta mensal
    var dailyGrossRateProfit: Double      // Rentabilidade bruta diária
    var taxesRate: Double                                 // Faixa do IR (%)
    var rateProfit: Decimal                               // Rentabilidade no período
    var annualNetRateProfit: Double                       // Rentabilidade líquida anual

}
