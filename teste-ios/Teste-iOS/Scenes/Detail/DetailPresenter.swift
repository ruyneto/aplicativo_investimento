import Foundation
class DetailPresenter {
    private var information: CorrectInformation
    private var detailService: DetailServiceType
    weak var viewController: DetailViewControllerType?
    init(information: CorrectInformation, service: DetailServiceType = DetailService()) {
        self.detailService = service
        self.information = information
    }

    private func getDetail() {
        detailService.getSimulation(correctInformation: information) { [weak self] in
            switch $0 {
            case .success(let response):
                self?.prepareResponse(response: response)
            case .error(let message):
                self?.viewController?.presentError(with: message)
            }
        }
    }

    private func prepareResponse(response: DetailResponse) {
        let viewModel = DetailResponseAdapter.adapt(response: response)
            self.viewController?.show(viewModel: viewModel)
    }
}

extension DetailPresenter: DetailPresenterType {
    func viewDidLoad() {
        getDetail()
    }
}
