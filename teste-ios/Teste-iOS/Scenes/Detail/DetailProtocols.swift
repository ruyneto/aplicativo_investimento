import Foundation

protocol DetailViewControllerType: AnyObject {
    func presentError(with message: String)
    func show(viewModel: DetailViewModel)
}

protocol DetailViewControllerDelegateType: AnyObject {
    func goToSimulateAgain()
}

protocol DetailPresenterType {
    func viewDidLoad()
}

protocol DetailServiceType {
    func getSimulation(
        correctInformation: CorrectInformation,
        callBack:@escaping ((ResponsePattern<DetailResponse>) -> Void))
}
