import UIKit
import UIComponents
import SkeletonView
class DetailView: UIView {
    var didClickSimulateAgain: (() -> Void)?
    let resultLabel: Label = {
        let label = Label(textStyle: .body, textColor: .gray)
        label.text = "Resultado da simulação"
        label.isSkeletonable = true
        return label
    }()

    let valueResultLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "RS 244,90"
        label.font = label.font.withSize(24)
        return label
    }()

    let valueTotalResultLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Rendimento total de RS 24,90"
        return label
    }()

    let firstValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Valor aplicado inicialmente"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    let bruteValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Valor bruto do investimento"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    let rendimentValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Valor do rendimento"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }()

    let rendimentTaxesinInvestmentLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        label.text = "IR sobre o rendimento"
        label.textAlignment = .left
        return label
    }()

    let liquidValueLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Valor líquido do investimento"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let rescueDateLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Data de resgate"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let daysPassedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Dias corridos"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let monthlyRendimentLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Rendimento mensal"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let pencentageCDILabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Percentual do CDI do investimento"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let yearRentabilityLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Rentabilidade anual"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()

    let periodRentabilityLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "Rentabilidade no período"
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()
    ///Values
    let moneyFirstValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.text = "R$ 1.000.000,00"
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()

    let moneyBruteValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.text = "R$ 1.000.000,00"
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()

    let moneyRendimentValueAppliedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.text = "R$ 1.000,00"
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()

    let moneyRendimentTaxesinInvestmentLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.text = "R$ 1.000,00"
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()

    let moneyLiquidValueLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.numberOfLines = 1
        label.text = "R$ 1.000,00"
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()

    let moneyRescueDateLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "27/10/2018"
        label.numberOfLines = 1
        return label
    }()

    let moneyDaysPassedLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "365"
        label.numberOfLines = 1
        return label
    }()

    let moneyMonthlyRendimentLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "R$ 1.000,00"
        label.numberOfLines = 1
        return label
    }()

    let moneyPencentageCDILabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "131"
        label.numberOfLines = 1
        return label
    }()

    let moneyYearRentabilityLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "R$ 10000,00"
        label.numberOfLines = 1
        return label
    }()

    let moneyperiodRentabilityLabel: Label = {
        let label = Label(textStyle: .headline, textColor: .gray)
        label.text = "8,87%"
        label.numberOfLines = 1
        return label
    }()
    ///Button
    let goBackbutton: Button = {
        let button = Button()
        button.setTitle("Simular Novamente", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.7831626534, blue: 0.7029977441, alpha: 1)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewCode()
        self.isSkeletonable = true
        self.subviews.forEach({$0.isSkeletonable = true})
        self.showAnimatedGradientSkeleton()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func show(viewModel: DetailViewModel) {
        self.hideSkeleton()
        self.moneyRescueDateLabel.text = viewModel.rescueDay
        self.moneyFirstValueAppliedLabel.text = viewModel.appliedValue
        self.moneyBruteValueAppliedLabel.text = viewModel.bruteValue
        self.valueResultLabel.text = viewModel.result
        self.moneyperiodRentabilityLabel.text = viewModel.rentabilityPeriod
        self.moneyYearRentabilityLabel.text = viewModel.rentabilityYear
        self.moneyPencentageCDILabel.text = viewModel.cdiPercentual
        self.moneyMonthlyRendimentLabel.text = viewModel.monthlyRendiment
        self.moneyDaysPassedLabel.text = viewModel.daysPassed
        self.moneyLiquidValueLabel.text = viewModel.result
        self.moneyRendimentTaxesinInvestmentLabel.text = viewModel.taxes
        self.moneyRendimentValueAppliedLabel.text = viewModel.liquidValue
        self.showColloredLabelWith(value: viewModel.liquidValue)
    }

    private func showColloredLabelWith(value: String) {
        let originalPart = "Rendimento total de "
        let colored = originalPart+"\(value)"
        let font: UIFont = .preferredFont(forTextStyle: .headline)
        let mutableWord = NSMutableAttributedString(string: colored, attributes: [NSAttributedString.Key.font: font])
        let color = #colorLiteral(red: 0.3172235787, green: 0.7978232503, blue: 0.7710497975, alpha: 1)
        mutableWord.addAttribute(
        NSAttributedString.Key.foregroundColor,
        value: color,
        range: NSRange(location: originalPart.count,
                       length: colored.count - originalPart.count))
        valueTotalResultLabel.attributedText = mutableWord
    }
}

extension DetailView: ViewCodable {
    func buildHierarchy() {
        self.addSubview(goBackbutton)
        self.addSubview(resultLabel)
        self.addSubview(valueResultLabel)
        self.addSubview(valueTotalResultLabel)

        self.addSubview(firstValueAppliedLabel)
        self.addSubview(bruteValueAppliedLabel)
        self.addSubview(rendimentValueAppliedLabel)
        self.addSubview(rendimentTaxesinInvestmentLabel)
        self.addSubview(liquidValueLabel)
        self.addSubview(rescueDateLabel)
        self.addSubview(daysPassedLabel)
        self.addSubview(monthlyRendimentLabel)
        self.addSubview(pencentageCDILabel)
        self.addSubview(yearRentabilityLabel)
        self.addSubview(periodRentabilityLabel)

        self.addSubview(moneyFirstValueAppliedLabel)
        self.addSubview(moneyRendimentTaxesinInvestmentLabel)
        self.addSubview(moneyBruteValueAppliedLabel)
        self.addSubview(moneyRendimentValueAppliedLabel)
        self.addSubview(moneyLiquidValueLabel)
        self.addSubview(moneyRescueDateLabel)
        self.addSubview(moneyDaysPassedLabel)
        self.addSubview(moneyMonthlyRendimentLabel)
        self.addSubview(moneyPencentageCDILabel)
        self.addSubview(moneyYearRentabilityLabel)
        self.addSubview(moneyperiodRentabilityLabel)
    }

    func buildConstraints() {
        self.goBackbutton.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(12)
            make.bottom.bottomMargin.equalToSuperview().inset(30)
        }

        self.resultLabel.snp.makeConstraints { make in
            if #available(iOS 11, *) {
                make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(30)
            } else {
                make.top.equalTo(self).inset(30)
            }
            make.left.right.equalToSuperview().inset(12)
        }

        self.valueResultLabel.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(12)
            make.top.equalTo(resultLabel).inset(30)
        }

        self.valueTotalResultLabel.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(12)
            make.top.equalTo(valueResultLabel).inset(30)
        }

        self.firstValueAppliedLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(valueTotalResultLabel.snp_bottomMargin).offset(40)
        }

        self.moneyFirstValueAppliedLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.left.equalTo(self.firstValueAppliedLabel.snp_rightMargin)
            make.top.equalTo(self.valueTotalResultLabel.snp_bottomMargin).offset(40)
        }

        self.bruteValueAppliedLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.firstValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.moneyBruteValueAppliedLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.left.equalTo(self.bruteValueAppliedLabel.snp_rightMargin)
            make.top.equalTo(self.firstValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.rendimentValueAppliedLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.bruteValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.moneyRendimentValueAppliedLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.left.equalTo(self.rendimentValueAppliedLabel.snp_rightMargin)
            make.top.equalTo(self.bruteValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.rendimentTaxesinInvestmentLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.rendimentValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.moneyRendimentTaxesinInvestmentLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.left.equalTo(self.rendimentTaxesinInvestmentLabel.snp_rightMargin)
            make.top.equalTo(self.rendimentValueAppliedLabel.snp_bottomMargin).offset(20)
        }

        self.liquidValueLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.rendimentTaxesinInvestmentLabel.snp_bottomMargin).offset(20)
        }

        self.moneyLiquidValueLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.rendimentTaxesinInvestmentLabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.liquidValueLabel.snp_rightMargin)
        }

        self.rescueDateLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.liquidValueLabel.snp_bottomMargin).offset(40)
        }

        self.moneyRescueDateLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.liquidValueLabel.snp_bottomMargin).offset(40)
            make.left.equalTo(self.rescueDateLabel.snp_rightMargin)
        }

        self.daysPassedLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.rescueDateLabel.snp_bottomMargin).offset(20)
        }

        self.moneyDaysPassedLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.rescueDateLabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.daysPassedLabel.snp_rightMargin)
        }

        self.monthlyRendimentLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.daysPassedLabel.snp_bottomMargin).offset(20)
        }

        self.moneyMonthlyRendimentLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.daysPassedLabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.monthlyRendimentLabel.snp_rightMargin)
        }

        self.pencentageCDILabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.monthlyRendimentLabel.snp_bottomMargin).offset(20)
        }

        self.moneyPencentageCDILabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.monthlyRendimentLabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.pencentageCDILabel.snp_rightMargin)
        }

        self.yearRentabilityLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.pencentageCDILabel.snp_bottomMargin).offset(20)
        }

        self.moneyYearRentabilityLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.pencentageCDILabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.yearRentabilityLabel.snp_rightMargin)
        }

        self.periodRentabilityLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(12)
            make.top.equalTo(self.yearRentabilityLabel.snp_bottomMargin).offset(20)
        }

        self.moneyperiodRentabilityLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(12)
            make.top.equalTo(self.yearRentabilityLabel.snp_bottomMargin).offset(20)
            make.left.equalTo(self.periodRentabilityLabel.snp_rightMargin)
        }
    }

    func additionalSetup() {
        self.backgroundColor = .white
        setActions()
    }

    private func setActions() {
        self.goBackbutton.addTarget(self, action: #selector(clickedSimulateAgain), for: .touchDown)
    }

    @objc
     func clickedSimulateAgain() {
        self.didClickSimulateAgain?()
    }
}
