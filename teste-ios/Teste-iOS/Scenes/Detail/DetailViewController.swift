import UIKit

class DetailViewController: UIViewController {
    private var presenter: DetailPresenterType
    weak var delegate: DetailViewControllerDelegateType?
    private var containerView: DetailView
    private var errorView = ErrorView()
    init(view: DetailView = DetailView(frame: .zero), presenter: DetailPresenterType) {
        self.containerView = view
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        self.presenter.viewDidLoad()
        setCallbacks()
    }

    override func loadView() {
        self.view = containerView
    }

    private func setCallbacks() {
        self.containerView.didClickSimulateAgain = {
            self.delegate?.goToSimulateAgain()
        }
        self.errorView.didClickSimulateAgain = {
            self.delegate?.goToSimulateAgain()
        }
    }
}

extension DetailViewController: DetailViewControllerType {
    func show(viewModel: DetailViewModel) {
        self.containerView.show(viewModel: viewModel)
    }

    func presentError(with message: String) {
        self.view = errorView
        errorView.show(message: message)
    }

}
