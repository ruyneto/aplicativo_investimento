import Foundation

class FormMoneyPresenter {
    weak var viewController: FormMoneyViewControllerType?
}

extension FormMoneyPresenter: FormMoneyPresenterType {
    func wantToValidateData(information: Information) {
        guard let valueMoney = self.checkValue(value: information.value)
              else {
            viewController?.presentErrorWithMessage(message: "Você deve preencher o valor da Aplicação.")
            return
        }
        guard let date = self.checkDate(value: information.date) else {
            viewController?.presentErrorWithMessage(message: "Data Invalida.")
            return
        }
        guard date > Date() else {
            viewController?.presentErrorWithMessage(message: "A data deve ser maior que a data de hoje")
            return
        }

        guard let percentage = checkPercentage(value: information.percentage) else {
            viewController?.presentErrorWithMessage(message: "Porcentagem invalida")
            return
        }

        let correctInfo = CorrectInformation(value: valueMoney, date: date, percentage: percentage)
        viewController?.doGoToDetail(with: correctInfo)
    }

    private func checkValue(value: String) -> Decimal? {
        let onlyNumber = value.filter("0123456789".contains)
        if onlyNumber.count == 0 {
            return nil
        }
        guard let decimal = Decimal(string: onlyNumber) else {return nil}
        return decimal/100.0
    }

    private func checkDate(value: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: value)
    }

    private func checkPercentage(value: String) -> Int? {
        let onlyNumber = value.filter("0123456789".contains)
        if onlyNumber.count < 1 {
            return nil
        }
        guard let integer = Int(onlyNumber) else {return nil}
        return integer
    }
}
