import Foundation

protocol FormMoneyViewControllerDelegate: AnyObject {
    func showDetailWith(information: CorrectInformation)
}

protocol FormMoneyViewControllerType: AnyObject {
    func doGoToDetail(with info: CorrectInformation)
    func presentErrorWithMessage(message: String)
}

protocol FormMoneyPresenterType: AnyObject {
    func wantToValidateData(information: Information)
}
