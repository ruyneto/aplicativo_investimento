import UIKit
import UIComponents
import SnapKit
class FormMoneyView: UIView {
    var doWantToCalculate: ((_:Information) -> Void)?
    var doClickedOutsideForm: (() -> Void)?
    private let buttonSend: Button = {
        let view = Button()
        view.setTitle("Calcular", for: .normal)
        return view
    }()

    private let applyValueInput: InputView = {
        let view = InputView()
        view.textfield.keyboardType = .numberPad
        view.title = "Quanto você gostaria de aplicar?*"
        view.placeholder = "R$"
        return view
    }()

    private let endDayInput: InputView = {
        let view = InputView()
        view.textfield.keyboardType = .numberPad
        view.title = "Qual a data de vencimento do rendimento?*"
        view.placeholder = "dia/mês/ano"
        return view
    }()

    private let taxInput: InputView = {
        let view = InputView()
        view.textfield.keyboardType = .numberPad
        view.title = "Qual o percentual do CDI do investimento?*"
        view.placeholder = "100%"
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewCode()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FormMoneyView: ViewCodable {
    func buildHierarchy() {
        self.addSubview(applyValueInput)
        self.addSubview(endDayInput)
        self.addSubview(taxInput)
        self.addSubview(buttonSend)
    }

    func buildConstraints() {
        applyValueInput.snp.makeConstraints { make in
            if #available(iOS 11, *) {
                make.top.equalTo(safeAreaLayoutGuide.snp.topMargin).inset(30)
            } else {
                make.top.equalTo(self).inset(30)
            }
            make.left.right.equalToSuperview().inset(12)
        }

        endDayInput.snp.makeConstraints { make in
            make.top.equalTo(applyValueInput.snp_bottomMargin).offset(30)
            make.left.right.equalToSuperview().inset(12)
        }

        taxInput.snp.makeConstraints { make in
            make.top.equalTo(endDayInput.snp_bottomMargin).offset(30)
            make.left.right.equalToSuperview().inset(12)
        }

        buttonSend.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(12)
            make.bottom.bottomMargin.equalToSuperview().inset(30)
        }
    }

    func additionalSetup() {
        self.backgroundColor = .white
        setupActions()
    }

    private func setupActions() {
        self.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapOutsideForm))
        self.addGestureRecognizer(tap)
        self.buttonSend.addTarget(self, action: #selector(didTapCalculateButton), for: .touchDown)
        self.applyValueInput.textfield.addTarget(self, action: #selector(didChangeValueMoney), for: .editingChanged)
        self.endDayInput.textfield.addTarget(self, action: #selector(didChangeValueDATE), for: .editingChanged)
        self.taxInput.textfield.addTarget(self, action: #selector(didChangeValuePercentage), for: .editingChanged)
    }

    @objc
    private func didChangeValueMoney(_ sender: UITextField) {
        sender.text = sender.text?.formatToMoney()
    }

    @objc
    private func didChangeValueDATE(_ sender: UITextField) {
        sender.text = sender.text?.formatDate()
    }

    @objc
    private func didChangeValuePercentage(_ sender: UITextField) {
        sender.text = sender.text?.formatPercentage()
    }

    @objc
    private func didTapCalculateButton() {
        let information = Information(
            value: applyValueInput.textfield.text ?? "",
            date: endDayInput.textfield.text ?? "",
            percentage: taxInput.textfield.text ?? "")
        self.doWantToCalculate?(information)
    }

    @objc private func didTapOutsideForm() {
        self.doClickedOutsideForm?()
    }
}
