import UIKit

class FormMoneyViewController: UIViewController {

    var containerView: FormMoneyView
    weak var delegate: FormMoneyViewControllerDelegate?
    private var presenter: FormMoneyPresenterType
    init(
        containerView: FormMoneyView = FormMoneyView(frame: .zero),
        presenter: FormMoneyPresenterType
    ) {
        self.containerView = containerView
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        self.view = containerView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setCallbacks()
    }

    private func hideKeyBoard() {
        view.endEditing(true)
    }

    private func setCallbacks() {
        self.containerView.doWantToCalculate = {
            self.presenter.wantToValidateData(information: $0)
        }
        self.containerView.doClickedOutsideForm = {
            self.hideKeyBoard()
        }
    }
}

extension FormMoneyViewController: FormMoneyViewControllerType {
    func doGoToDetail(with info: CorrectInformation) {
        self.delegate?.showDetailWith(information: info)
    }

    func presentErrorWithMessage(message: String) {
        let alert = UIAlertController(title: "Ops", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
