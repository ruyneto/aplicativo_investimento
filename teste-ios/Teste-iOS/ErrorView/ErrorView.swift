import UIKit
import UIComponents
import SnapKit
class ErrorView: UIView {
    var didClickSimulateAgain: (() -> Void)?
    let messageLabel: Label = {
        let view = Label(textStyle: .body, textColor: .black)
        view.text = "Error Genérico"
        return view
    }()

    private let buttonGoBack: Button = {
        let view = Button()
        view.setTitle("Voltar", for: .normal)
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewCode()
        setActions()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func show(message: String) {
        self.messageLabel.text = message
    }
}

extension ErrorView: ViewCodable {
    func buildHierarchy() {
        self.addSubview(messageLabel)
        self.addSubview(buttonGoBack)
    }

    func buildConstraints() {
        messageLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.left.right.equalToSuperview().inset(16)
        }

        buttonGoBack.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(12)
            make.top.equalTo(messageLabel.snp_bottomMargin).offset(30)
        }
    }

    func additionalSetup() {
        self.backgroundColor = .white
    }

    private func setActions() {
        self.buttonGoBack.addTarget(self, action: #selector(doCallGoBack), for: .touchDown)
    }

    @objc
    private func doCallGoBack() {
        self.didClickSimulateAgain?()
    }
}
