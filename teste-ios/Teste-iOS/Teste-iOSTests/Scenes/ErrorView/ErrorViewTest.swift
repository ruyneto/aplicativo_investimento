import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class ErrorViewTest: QuickSpec {
    override func spec() {
        var sut: ErrorView!
        beforeEach {
            sut = ErrorView(frame: .zero)
        }
        describe("At show method") {
            it("has valid snapshot") {
                sut = ErrorView(frame: .zero)
                sut.frame = CGRect(x: 0, y: 0, width: 375, height: 667)
                sut.show(message: "Erro de Show")
                expect(sut).to(haveValidSnapshot())
            }
        }
    }
}
