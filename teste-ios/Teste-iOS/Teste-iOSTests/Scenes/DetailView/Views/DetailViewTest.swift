//swiftlint:disable all
import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class DetailViewsTest: QuickSpec {
    override func spec() {
        var sut: DetailView!
        beforeEach {
            sut = DetailView(frame: .zero)
        }
        describe("At show method") {
            it("has valid snapshot") {
                sut = DetailView(frame: .zero)
                let viewModel = DetailViewModel(
                    result: "YYY",
                    appliedValue: "YYY",
                    bruteValue: "YYY",
                    rendimentValue: "YYY",
                    taxes: "YYY",
                    liquidValue: "YYY",
                    rescueDay: "YYY",
                    daysPassed: "YYY",
                    monthlyRendiment: "YYY",
                    cdiPercentual: "YYY",
                    rentabilityYear: "YYY",
                    rentabilityPeriod: "YYY")
                sut.show(viewModel: viewModel)
                sut.frame = CGRect(x: 0, y: 0, width: 375, height: 667)
                expect(sut).to(haveValidSnapshot())
            }
        }
        
        describe("At clickedSimulateAgain method") {
            it("has called viewController delegate goToSimulateAgain ") {
                sut = DetailView(frame: .zero)
//                let viewModel = DetailViewModel(
//                    result: "YYY",
//                    appliedValue: "YYY",
//                    bruteValue: "YYY",
//                    rendimentValue: "YYY",
//                    taxes: "YYY",
//                    liquidValue: "YYY",
//                    rescueDay: "YYY",
//                    daysPassed: "YYY",
//                    monthlyRendiment: "YYY",
//                    cdiPercentual: "YYY",
//                    rentabilityYear: "YYY",
//                    rentabilityPeriod: "YYY")
//                sut.show(viewModel: viewModel)
//                sut.frame = CGRect(x: 0, y: 0, width: 375, height: 667)
                let delegate = MockDetailViewControllerDelegateType()
                let presenter = MockDetailPresenterType()
                let viewController = DetailViewController(view: sut, presenter: presenter)
                viewController.delegate = delegate
                stub(presenter) {stub in
                    when(stub).viewDidLoad().thenDoNothing()
                }
                stub(delegate) {stub in
                    when(stub).goToSimulateAgain().thenDoNothing()
                }
                viewController.viewDidLoad()
                sut.clickedSimulateAgain()
                verify(delegate).goToSimulateAgain()
            }
        }
    }
}
