//swiftlint:disable function_body_length
import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class DetailViewControllerTest: QuickSpec {
    override func spec() {
        var view: DetailView!
        var sut: DetailViewController!
        var presenter: MockDetailPresenterType!
        var delegate: MockDetailViewControllerDelegateType!
        beforeEach {
            delegate = MockDetailViewControllerDelegateType()
            view = DetailView(frame: .zero)
            presenter = MockDetailPresenterType()
            sut = DetailViewController(view: view, presenter: presenter)
        }

        describe("At viewDidLoad") {
            it("has set callBacks") {
                stub(presenter) { stub in
                    when(stub.viewDidLoad()).thenDoNothing()
                }
                sut.viewDidLoad()
                expect(view.didClickSimulateAgain == nil).to(beFalse())
            }
        }

        describe("At view didClickSimulateAgain") {
            it("do presenter viewDidLoad") {
                stub(presenter) { stub in
                    when(stub.viewDidLoad()).thenDoNothing()
                }
                sut.viewDidLoad()
                view.didClickSimulateAgain!()
                verify(presenter).viewDidLoad()
            }
        }

        describe("At sut show") {
            it("has called view show") {
                stub(presenter) { stub in
                    when(stub.viewDidLoad()).thenDoNothing()
                }
                sut.viewDidLoad()
                let viewMock = MockDetailView(frame: .zero)
                let vcDumb   = DetailViewController(view: viewMock, presenter: presenter)
                stub(viewMock) { stub in
                    when(stub.show(viewModel: any())).thenDoNothing()
                }
                vcDumb.show(viewModel:
                                DetailViewModel(result: "",
                                                appliedValue: "",
                                                bruteValue: "",
                                                rendimentValue: "",
                                                taxes: "",
                                                liquidValue: "",
                                                rescueDay: "",
                                                daysPassed: "",
                                                monthlyRendiment: "",
                                                cdiPercentual: "",
                                                rentabilityYear: "",
                                                rentabilityPeriod: "")
                )
                verify(viewMock).show(viewModel: any())
            }
        }

        describe("At sut presentError") {
            it("has set errorView") {
                sut.presentError(with: "")
                expect(view).notTo(equal(sut.view))
            }
        }

        describe("At sut loadView") {
            it("has set view") {
                sut.loadView()
                expect(view).to(equal(sut.view))
            }
        }

        describe("At sut idClickSimulateAgain") {
            it("has called delegate goToSimulateAgain") {
                stub(presenter) { stub in
                    when(stub.viewDidLoad()).thenDoNothing()
                }
                sut.viewDidLoad()
                sut.delegate = delegate
                stub(delegate) { stub in
                    when(stub.goToSimulateAgain()).thenDoNothing()
                }
                view.didClickSimulateAgain!()
                verify(delegate).goToSimulateAgain()
            }
        }
    }
}
