//swiftlint:disable all
import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class DetailPresenterTest: QuickSpec {
    override func spec() {
        var sut: DetailPresenter!
        var viewController: MockDetailViewControllerType!
        var repository: MockDetailServiceType!
        beforeEach {
            viewController = MockDetailViewControllerType()
            repository = MockDetailServiceType()
            let info = CorrectInformation(value: 1.1, date: Date(), percentage: 1)
            sut = DetailPresenter(information: info, service: repository)
            sut.viewController = viewController
        }
        describe("At presenter viewDidLoad perfect flux ") {
            it("has called viewController show") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.show(viewModel: any()).thenDoNothing()
                }
                stub(repository){stub in
                    when(stub.getSimulation(correctInformation: any(), callBack: any())).then {correct, callBack in
                        let response = DetailResponse(
                            investmentParameter:
                                .init(investedAmount: 1.1,
                                      yearlyInterestRate: 1.1,
                                      maturityTotalDays: 1,
                                      maturityBusinessDays: 1,
                                      maturityDate: "",
                                      rate: 1,
                                      isTaxFree: false),
                            grossAmount: 1.1,
                            taxesAmount: 1.1,
                            netAmount: 1.1,
                            grossAmountProfit: 1.1,
                            netAmountProfit: 1.1,
                            annualGrossRateProfit: 1.1,
                            monthlyGrossRateProfit: 1.1,
                            dailyGrossRateProfit: 1.1,
                            taxesRate: 1.1,
                            rateProfit: 1.1,
                            annualNetRateProfit: 1.1)
                        callBack(.success(response))
                    }
                }
                sut.viewDidLoad()
                verify(viewController).show(viewModel: any())
            }
        }
        describe("At presenter viewDidLoad imperfect flux ") {
            it("has called viewController error") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.presentError(with: any()).thenDoNothing()
                }
                stub(repository){stub in
                    when(stub.getSimulation(correctInformation: any(), callBack: any())).then {correct, callBack in
                        callBack(.error(""))
                    }
                }
                sut.viewDidLoad()
                verify(viewController).presentError(with: any())
            }
        }
    }
}
