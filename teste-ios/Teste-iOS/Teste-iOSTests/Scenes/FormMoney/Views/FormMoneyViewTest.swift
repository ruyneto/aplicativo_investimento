import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class FormMoneyTest: QuickSpec {
    override func spec() {
        var sut: FormMoneyView!
        beforeEach {
            sut = FormMoneyView(frame: .zero)
        }
        describe("At init method") {
            it("has valid snapshot") {
                sut.frame = CGRect(x: 0, y: 0, width: 375, height: 667)
                expect(sut).to(haveValidSnapshot())
            }
        }
    }
}
