//swiftlint:disable all
import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class FormMoneyPresenterTest: QuickSpec {
    override func spec() {
        var sut: FormMoneyPresenter!
        var viewController: MockFormMoneyViewControllerType!
        beforeEach {
            viewController = MockFormMoneyViewControllerType()
            sut = FormMoneyPresenter()
            sut.viewController = viewController
        }
        describe("At presenter wantToValidateData perfect flux ") {
            it("has viewController doGoToDetail") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.doGoToDetail(with: any()).thenDoNothing()
                }
                let information = Information(value: "R$ 12,50", date: "12/12/2200", percentage: "123%")
                sut.wantToValidateData(information: information)
                verify(viewController).doGoToDetail(with: any())
            }
        }
        
        describe("At presenter wantToValidateData imperfect flux value ") {
            it("has not viewController doGoToDetail") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.presentErrorWithMessage(message: any()).thenDoNothing()
                }
                let information = Information(value: "", date: "12/12/2200", percentage: "123%")
                sut.wantToValidateData(information: information)
                verify(viewController,times(0)).doGoToDetail(with: any())
            }
        }
        
        describe("At presenter wantToValidateData imperfect flux date ") {
            it("has not viewController doGoToDetail") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.presentErrorWithMessage(message: any()).thenDoNothing()
                }
                let information = Information(value: "R$ 12,50", date: "12/12/1222", percentage: "123%")
                sut.wantToValidateData(information: information)
                verify(viewController,times(0)).doGoToDetail(with: any())
            }
        }
        
        describe("At presenter wantToValidateData imperfect flux date invalid ") {
            it("has not viewController doGoToDetail") {
                sut.viewController = viewController
                stub(viewController){stub in
                    stub.presentErrorWithMessage(message: any()).thenDoNothing()
                }
                let information = Information(value: "R$ 12,50", date: "A/A/A", percentage: "123%")
                sut.wantToValidateData(information: information)
                verify(viewController,times(0)).doGoToDetail(with: any())
            }
        }

        describe("At presenter wantToValidateData imperfect flux percentage ") {
            it("has not viewController doGoToDetail") {
                sut.viewController = viewController
                let information = Information(value: "R$ 12,50", date: "12/12/2200", percentage: "N")
                stub(viewController){stub in
                    stub.doGoToDetail(with: any()).thenDoNothing()
                    stub.presentErrorWithMessage(message: any()).thenDoNothing()
                }
                sut.wantToValidateData(information: information)
                verify(viewController,times(0)).doGoToDetail(with: any())
            }
        }
    }
}
