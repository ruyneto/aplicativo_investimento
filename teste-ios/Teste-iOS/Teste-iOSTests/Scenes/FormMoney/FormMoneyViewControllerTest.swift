// swiftlint:disable function_body_length

import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class FormMoneyViewControllerTest: QuickSpec {
    override func spec() {
        var view: FormMoneyView!
        var sut: FormMoneyViewController!
        var presenter: MockFormMoneyPresenterType!
        beforeEach {
            view = FormMoneyView(frame: .zero)
            presenter = MockFormMoneyPresenterType()
            sut = FormMoneyViewController(containerView: view, presenter: presenter)
        }

        describe("At viewDidLoad") {
            it("has set callBacks") {
                sut.viewDidLoad()
                expect(view.doWantToCalculate == nil).to(beFalse())
                expect(view.doClickedOutsideForm == nil).to(beFalse())
            }
        }

        describe("At view doWantToCalculate") {
            it("do call wantToValidateData") {
                let info = Information(value: "", date: "", percentage: "")
                sut.viewDidLoad()
                stub(presenter) { stub in
                    when(stub.wantToValidateData(information: any())).thenDoNothing()
                }
                view.doWantToCalculate!(info)
                verify(presenter).wantToValidateData(information: any())
            }
        }

        describe("At view doClickedOutsideForm") {
            it("has called hide KeyBoard") {
                sut.viewDidLoad()
                view.doClickedOutsideForm!()
            }
        }

        describe("At view doWantCalculate") {
            it("has called presenter wantToValidateDate") {
                sut.viewDidLoad()
                let correctInfo = Information(value: "", date: "", percentage: "")
                stub(presenter) { stub in
                    stub.wantToValidateData(information: any()).thenDoNothing()
                }
                view.doWantToCalculate!(correctInfo)
                verify(presenter).wantToValidateData(information: any())
            }
        }

        describe("At view doWantCalculate") {
            it("has called presenter wantToValidateDate") {
                sut.viewDidLoad()
                let correctInfo = Information(value: "", date: "", percentage: "")
                stub(presenter) { stub in
                    stub.wantToValidateData(information: any()).thenDoNothing()
                }
                view.doWantToCalculate!(correctInfo)
                verify(presenter).wantToValidateData(information: any())
            }
        }

        describe("At doGoToDetail") {
            it("has called delegate showDetailWith") {
                sut.viewDidLoad()
                let delegate = MockFormMoneyViewControllerDelegate()
                let correctInfo = CorrectInformation(value: 1.1, date: Date(), percentage: 1)
                sut.delegate = delegate
                stub(delegate) { stub in
                    stub.showDetailWith(information: any()).thenDoNothing()
                }
                sut.doGoToDetail(with: correctInfo)
                verify(delegate).showDetailWith(information: any())
            }
        }

        describe("At presentErrorWithMessage") {
            it("has Alert") {
                sut.viewDidLoad()
                sut.presentErrorWithMessage(message: "AAA")
            }
        }
    }
}
