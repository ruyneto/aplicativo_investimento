import Quick
import Nimble
import Cuckoo
import Nimble_Snapshots
import Foundation
@testable import Teste_iOS

class CoordinatorTests: QuickSpec {
    override func spec() {
        var sut: Coordinator!
        var navigationController: UINavigationController!
        beforeEach {
            navigationController = UINavigationController()
            sut = Coordinator(navigationController: navigationController)
        }

        describe("At showDetailWith method") {
            it("has valid ViewController") {
                let correctInformation = CorrectInformation(value: 1.12, date: Date(), percentage: 1)
                sut.showDetailWith(information: correctInformation)
                expect(navigationController.viewControllers.count).to(equal(1))
            }
        }

        describe("At goToSimulateAgain method") {
            it("has 0 ViewController") {
                let viewController = UIViewController()
                navigationController.pushViewController(viewController, animated: false)
                sut.goToSimulateAgain()
                expect(navigationController.viewControllers.count).to(equal(1))
            }
        }
    }
}
