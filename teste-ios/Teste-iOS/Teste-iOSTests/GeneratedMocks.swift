//swiftlint:disable all 
 // MARK: - Mocks generated from file: Scenes/Detail/DetailPresenter.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import Foundation


 class MockDetailPresenter: DetailPresenter, Cuckoo.ClassMock {
    
     typealias MocksType = DetailPresenter
    
     typealias Stubbing = __StubbingProxy_DetailPresenter
     typealias Verification = __VerificationProxy_DetailPresenter

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: DetailPresenter?

     func enableDefaultImplementation(_ stub: DetailPresenter) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var viewController: DetailViewControllerType? {
        get {
            return cuckoo_manager.getter("viewController",
                superclassCall:
                    
                    super.viewController
                    ,
                defaultCall: __defaultImplStub!.viewController)
        }
        
        set {
            cuckoo_manager.setter("viewController",
                value: newValue,
                superclassCall:
                    
                    super.viewController = newValue
                    ,
                defaultCall: __defaultImplStub!.viewController = newValue)
        }
        
    }
    

    

    

	 struct __StubbingProxy_DetailPresenter: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var viewController: Cuckoo.ClassToBeStubbedOptionalProperty<MockDetailPresenter, DetailViewControllerType> {
	        return .init(manager: cuckoo_manager, name: "viewController")
	    }
	    
	    
	}

	 struct __VerificationProxy_DetailPresenter: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var viewController: Cuckoo.VerifyOptionalProperty<DetailViewControllerType> {
	        return .init(manager: cuckoo_manager, name: "viewController", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	}
}

 class DetailPresenterStub: DetailPresenter {
    
    
     override var viewController: DetailViewControllerType? {
        get {
            return DefaultValueRegistry.defaultValue(for: (DetailViewControllerType?).self)
        }
        
        set { }
        
    }
    

    

    
}


// MARK: - Mocks generated from file: Scenes/Detail/DetailProtocols.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import Foundation


 class MockDetailViewControllerType: DetailViewControllerType, Cuckoo.ProtocolMock {
    
     typealias MocksType = DetailViewControllerType
    
     typealias Stubbing = __StubbingProxy_DetailViewControllerType
     typealias Verification = __VerificationProxy_DetailViewControllerType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DetailViewControllerType?

     func enableDefaultImplementation(_ stub: DetailViewControllerType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func presentError(with message: String)  {
        
    return cuckoo_manager.call("presentError(with: String)",
            parameters: (message),
            escapingParameters: (message),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.presentError(with: message))
        
    }
    
    
    
     func show(viewModel: DetailViewModel)  {
        
    return cuckoo_manager.call("show(viewModel: DetailViewModel)",
            parameters: (viewModel),
            escapingParameters: (viewModel),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.show(viewModel: viewModel))
        
    }
    

	 struct __StubbingProxy_DetailViewControllerType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func presentError<M1: Cuckoo.Matchable>(with message: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(String)> where M1.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailViewControllerType.self, method: "presentError(with: String)", parameterMatchers: matchers))
	    }
	    
	    func show<M1: Cuckoo.Matchable>(viewModel: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(DetailViewModel)> where M1.MatchedType == DetailViewModel {
	        let matchers: [Cuckoo.ParameterMatcher<(DetailViewModel)>] = [wrap(matchable: viewModel) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailViewControllerType.self, method: "show(viewModel: DetailViewModel)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailViewControllerType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func presentError<M1: Cuckoo.Matchable>(with message: M1) -> Cuckoo.__DoNotUse<(String), Void> where M1.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
	        return cuckoo_manager.verify("presentError(with: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func show<M1: Cuckoo.Matchable>(viewModel: M1) -> Cuckoo.__DoNotUse<(DetailViewModel), Void> where M1.MatchedType == DetailViewModel {
	        let matchers: [Cuckoo.ParameterMatcher<(DetailViewModel)>] = [wrap(matchable: viewModel) { $0 }]
	        return cuckoo_manager.verify("show(viewModel: DetailViewModel)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailViewControllerTypeStub: DetailViewControllerType {
    

    

    
     func presentError(with message: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func show(viewModel: DetailViewModel)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockDetailViewControllerDelegateType: DetailViewControllerDelegateType, Cuckoo.ProtocolMock {
    
     typealias MocksType = DetailViewControllerDelegateType
    
     typealias Stubbing = __StubbingProxy_DetailViewControllerDelegateType
     typealias Verification = __VerificationProxy_DetailViewControllerDelegateType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DetailViewControllerDelegateType?

     func enableDefaultImplementation(_ stub: DetailViewControllerDelegateType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func goToSimulateAgain()  {
        
    return cuckoo_manager.call("goToSimulateAgain()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.goToSimulateAgain())
        
    }
    

	 struct __StubbingProxy_DetailViewControllerDelegateType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func goToSimulateAgain() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailViewControllerDelegateType.self, method: "goToSimulateAgain()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailViewControllerDelegateType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func goToSimulateAgain() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("goToSimulateAgain()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailViewControllerDelegateTypeStub: DetailViewControllerDelegateType {
    

    

    
     func goToSimulateAgain()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockDetailPresenterType: DetailPresenterType, Cuckoo.ProtocolMock {
    
     typealias MocksType = DetailPresenterType
    
     typealias Stubbing = __StubbingProxy_DetailPresenterType
     typealias Verification = __VerificationProxy_DetailPresenterType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DetailPresenterType?

     func enableDefaultImplementation(_ stub: DetailPresenterType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func viewDidLoad()  {
        
    return cuckoo_manager.call("viewDidLoad()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.viewDidLoad())
        
    }
    

	 struct __StubbingProxy_DetailPresenterType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func viewDidLoad() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailPresenterType.self, method: "viewDidLoad()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailPresenterType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func viewDidLoad() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("viewDidLoad()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailPresenterTypeStub: DetailPresenterType {
    

    

    
     func viewDidLoad()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockDetailServiceType: DetailServiceType, Cuckoo.ProtocolMock {
    
     typealias MocksType = DetailServiceType
    
     typealias Stubbing = __StubbingProxy_DetailServiceType
     typealias Verification = __VerificationProxy_DetailServiceType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: DetailServiceType?

     func enableDefaultImplementation(_ stub: DetailServiceType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func getSimulation(correctInformation: CorrectInformation, callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void))  {
        
    return cuckoo_manager.call("getSimulation(correctInformation: CorrectInformation, callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void))",
            parameters: (correctInformation, callBack),
            escapingParameters: (correctInformation, callBack),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.getSimulation(correctInformation: correctInformation, callBack: callBack))
        
    }
    

	 struct __StubbingProxy_DetailServiceType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func getSimulation<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(correctInformation: M1, callBack: M2) -> Cuckoo.ProtocolStubNoReturnFunction<(CorrectInformation, ((ResponsePattern<DetailResponse>) -> Void))> where M1.MatchedType == CorrectInformation, M2.MatchedType == ((ResponsePattern<DetailResponse>) -> Void) {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation, ((ResponsePattern<DetailResponse>) -> Void))>] = [wrap(matchable: correctInformation) { $0.0 }, wrap(matchable: callBack) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailServiceType.self, method: "getSimulation(correctInformation: CorrectInformation, callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void))", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailServiceType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func getSimulation<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(correctInformation: M1, callBack: M2) -> Cuckoo.__DoNotUse<(CorrectInformation, ((ResponsePattern<DetailResponse>) -> Void)), Void> where M1.MatchedType == CorrectInformation, M2.MatchedType == ((ResponsePattern<DetailResponse>) -> Void) {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation, ((ResponsePattern<DetailResponse>) -> Void))>] = [wrap(matchable: correctInformation) { $0.0 }, wrap(matchable: callBack) { $0.1 }]
	        return cuckoo_manager.verify("getSimulation(correctInformation: CorrectInformation, callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void))", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailServiceTypeStub: DetailServiceType {
    

    

    
     func getSimulation(correctInformation: CorrectInformation, callBack: @escaping ((ResponsePattern<DetailResponse>) -> Void))   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: Scenes/Detail/DetailViewController.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import UIKit


 class MockDetailViewController: DetailViewController, Cuckoo.ClassMock {
    
     typealias MocksType = DetailViewController
    
     typealias Stubbing = __StubbingProxy_DetailViewController
     typealias Verification = __VerificationProxy_DetailViewController

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: DetailViewController?

     func enableDefaultImplementation(_ stub: DetailViewController) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var delegate: DetailViewControllerDelegateType? {
        get {
            return cuckoo_manager.getter("delegate",
                superclassCall:
                    
                    super.delegate
                    ,
                defaultCall: __defaultImplStub!.delegate)
        }
        
        set {
            cuckoo_manager.setter("delegate",
                value: newValue,
                superclassCall:
                    
                    super.delegate = newValue
                    ,
                defaultCall: __defaultImplStub!.delegate = newValue)
        }
        
    }
    

    

    
    
    
     override func viewDidLoad()  {
        
    return cuckoo_manager.call("viewDidLoad()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.viewDidLoad()
                ,
            defaultCall: __defaultImplStub!.viewDidLoad())
        
    }
    
    
    
     override func loadView()  {
        
    return cuckoo_manager.call("loadView()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.loadView()
                ,
            defaultCall: __defaultImplStub!.loadView())
        
    }
    

	 struct __StubbingProxy_DetailViewController: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var delegate: Cuckoo.ClassToBeStubbedOptionalProperty<MockDetailViewController, DetailViewControllerDelegateType> {
	        return .init(manager: cuckoo_manager, name: "delegate")
	    }
	    
	    
	    func viewDidLoad() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailViewController.self, method: "viewDidLoad()", parameterMatchers: matchers))
	    }
	    
	    func loadView() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailViewController.self, method: "loadView()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailViewController: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var delegate: Cuckoo.VerifyOptionalProperty<DetailViewControllerDelegateType> {
	        return .init(manager: cuckoo_manager, name: "delegate", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func viewDidLoad() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("viewDidLoad()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func loadView() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("loadView()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailViewControllerStub: DetailViewController {
    
    
     override var delegate: DetailViewControllerDelegateType? {
        get {
            return DefaultValueRegistry.defaultValue(for: (DetailViewControllerDelegateType?).self)
        }
        
        set { }
        
    }
    

    

    
     override func viewDidLoad()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func loadView()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: Scenes/Detail/Views/DetailView.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import SkeletonView
import UIComponents
import UIKit


 class MockDetailView: DetailView, Cuckoo.ClassMock {
    
     typealias MocksType = DetailView
    
     typealias Stubbing = __StubbingProxy_DetailView
     typealias Verification = __VerificationProxy_DetailView

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: DetailView?

     func enableDefaultImplementation(_ stub: DetailView) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var didClickSimulateAgain: (() -> Void)? {
        get {
            return cuckoo_manager.getter("didClickSimulateAgain",
                superclassCall:
                    
                    super.didClickSimulateAgain
                    ,
                defaultCall: __defaultImplStub!.didClickSimulateAgain)
        }
        
        set {
            cuckoo_manager.setter("didClickSimulateAgain",
                value: newValue,
                superclassCall:
                    
                    super.didClickSimulateAgain = newValue
                    ,
                defaultCall: __defaultImplStub!.didClickSimulateAgain = newValue)
        }
        
    }
    

    

    
    
    
     override func show(viewModel: DetailViewModel)  {
        
    return cuckoo_manager.call("show(viewModel: DetailViewModel)",
            parameters: (viewModel),
            escapingParameters: (viewModel),
            superclassCall:
                
                super.show(viewModel: viewModel)
                ,
            defaultCall: __defaultImplStub!.show(viewModel: viewModel))
        
    }
    

	 struct __StubbingProxy_DetailView: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var didClickSimulateAgain: Cuckoo.ClassToBeStubbedOptionalProperty<MockDetailView, (() -> Void)> {
	        return .init(manager: cuckoo_manager, name: "didClickSimulateAgain")
	    }
	    
	    
	    func show<M1: Cuckoo.Matchable>(viewModel: M1) -> Cuckoo.ClassStubNoReturnFunction<(DetailViewModel)> where M1.MatchedType == DetailViewModel {
	        let matchers: [Cuckoo.ParameterMatcher<(DetailViewModel)>] = [wrap(matchable: viewModel) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockDetailView.self, method: "show(viewModel: DetailViewModel)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_DetailView: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var didClickSimulateAgain: Cuckoo.VerifyOptionalProperty<(() -> Void)> {
	        return .init(manager: cuckoo_manager, name: "didClickSimulateAgain", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func show<M1: Cuckoo.Matchable>(viewModel: M1) -> Cuckoo.__DoNotUse<(DetailViewModel), Void> where M1.MatchedType == DetailViewModel {
	        let matchers: [Cuckoo.ParameterMatcher<(DetailViewModel)>] = [wrap(matchable: viewModel) { $0 }]
	        return cuckoo_manager.verify("show(viewModel: DetailViewModel)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class DetailViewStub: DetailView {
    
    
     override var didClickSimulateAgain: (() -> Void)? {
        get {
            return DefaultValueRegistry.defaultValue(for: ((() -> Void)?).self)
        }
        
        set { }
        
    }
    

    

    
     override func show(viewModel: DetailViewModel)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: Scenes/FormMoney/FormMoneyPresenter.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import Foundation


 class MockFormMoneyPresenter: FormMoneyPresenter, Cuckoo.ClassMock {
    
     typealias MocksType = FormMoneyPresenter
    
     typealias Stubbing = __StubbingProxy_FormMoneyPresenter
     typealias Verification = __VerificationProxy_FormMoneyPresenter

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: FormMoneyPresenter?

     func enableDefaultImplementation(_ stub: FormMoneyPresenter) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var viewController: FormMoneyViewControllerType? {
        get {
            return cuckoo_manager.getter("viewController",
                superclassCall:
                    
                    super.viewController
                    ,
                defaultCall: __defaultImplStub!.viewController)
        }
        
        set {
            cuckoo_manager.setter("viewController",
                value: newValue,
                superclassCall:
                    
                    super.viewController = newValue
                    ,
                defaultCall: __defaultImplStub!.viewController = newValue)
        }
        
    }
    

    

    

	 struct __StubbingProxy_FormMoneyPresenter: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var viewController: Cuckoo.ClassToBeStubbedOptionalProperty<MockFormMoneyPresenter, FormMoneyViewControllerType> {
	        return .init(manager: cuckoo_manager, name: "viewController")
	    }
	    
	    
	}

	 struct __VerificationProxy_FormMoneyPresenter: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var viewController: Cuckoo.VerifyOptionalProperty<FormMoneyViewControllerType> {
	        return .init(manager: cuckoo_manager, name: "viewController", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	}
}

 class FormMoneyPresenterStub: FormMoneyPresenter {
    
    
     override var viewController: FormMoneyViewControllerType? {
        get {
            return DefaultValueRegistry.defaultValue(for: (FormMoneyViewControllerType?).self)
        }
        
        set { }
        
    }
    

    

    
}


// MARK: - Mocks generated from file: Scenes/FormMoney/FormMoneyProtocols.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import Foundation


 class MockFormMoneyViewControllerDelegate: FormMoneyViewControllerDelegate, Cuckoo.ProtocolMock {
    
     typealias MocksType = FormMoneyViewControllerDelegate
    
     typealias Stubbing = __StubbingProxy_FormMoneyViewControllerDelegate
     typealias Verification = __VerificationProxy_FormMoneyViewControllerDelegate

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: FormMoneyViewControllerDelegate?

     func enableDefaultImplementation(_ stub: FormMoneyViewControllerDelegate) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func showDetailWith(information: CorrectInformation)  {
        
    return cuckoo_manager.call("showDetailWith(information: CorrectInformation)",
            parameters: (information),
            escapingParameters: (information),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.showDetailWith(information: information))
        
    }
    

	 struct __StubbingProxy_FormMoneyViewControllerDelegate: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func showDetailWith<M1: Cuckoo.Matchable>(information: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(CorrectInformation)> where M1.MatchedType == CorrectInformation {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation)>] = [wrap(matchable: information) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyViewControllerDelegate.self, method: "showDetailWith(information: CorrectInformation)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_FormMoneyViewControllerDelegate: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func showDetailWith<M1: Cuckoo.Matchable>(information: M1) -> Cuckoo.__DoNotUse<(CorrectInformation), Void> where M1.MatchedType == CorrectInformation {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation)>] = [wrap(matchable: information) { $0 }]
	        return cuckoo_manager.verify("showDetailWith(information: CorrectInformation)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class FormMoneyViewControllerDelegateStub: FormMoneyViewControllerDelegate {
    

    

    
     func showDetailWith(information: CorrectInformation)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockFormMoneyViewControllerType: FormMoneyViewControllerType, Cuckoo.ProtocolMock {
    
     typealias MocksType = FormMoneyViewControllerType
    
     typealias Stubbing = __StubbingProxy_FormMoneyViewControllerType
     typealias Verification = __VerificationProxy_FormMoneyViewControllerType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: FormMoneyViewControllerType?

     func enableDefaultImplementation(_ stub: FormMoneyViewControllerType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func doGoToDetail(with info: CorrectInformation)  {
        
    return cuckoo_manager.call("doGoToDetail(with: CorrectInformation)",
            parameters: (info),
            escapingParameters: (info),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.doGoToDetail(with: info))
        
    }
    
    
    
     func presentErrorWithMessage(message: String)  {
        
    return cuckoo_manager.call("presentErrorWithMessage(message: String)",
            parameters: (message),
            escapingParameters: (message),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.presentErrorWithMessage(message: message))
        
    }
    

	 struct __StubbingProxy_FormMoneyViewControllerType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func doGoToDetail<M1: Cuckoo.Matchable>(with info: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(CorrectInformation)> where M1.MatchedType == CorrectInformation {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation)>] = [wrap(matchable: info) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyViewControllerType.self, method: "doGoToDetail(with: CorrectInformation)", parameterMatchers: matchers))
	    }
	    
	    func presentErrorWithMessage<M1: Cuckoo.Matchable>(message: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(String)> where M1.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyViewControllerType.self, method: "presentErrorWithMessage(message: String)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_FormMoneyViewControllerType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func doGoToDetail<M1: Cuckoo.Matchable>(with info: M1) -> Cuckoo.__DoNotUse<(CorrectInformation), Void> where M1.MatchedType == CorrectInformation {
	        let matchers: [Cuckoo.ParameterMatcher<(CorrectInformation)>] = [wrap(matchable: info) { $0 }]
	        return cuckoo_manager.verify("doGoToDetail(with: CorrectInformation)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func presentErrorWithMessage<M1: Cuckoo.Matchable>(message: M1) -> Cuckoo.__DoNotUse<(String), Void> where M1.MatchedType == String {
	        let matchers: [Cuckoo.ParameterMatcher<(String)>] = [wrap(matchable: message) { $0 }]
	        return cuckoo_manager.verify("presentErrorWithMessage(message: String)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class FormMoneyViewControllerTypeStub: FormMoneyViewControllerType {
    

    

    
     func doGoToDetail(with info: CorrectInformation)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     func presentErrorWithMessage(message: String)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}



 class MockFormMoneyPresenterType: FormMoneyPresenterType, Cuckoo.ProtocolMock {
    
     typealias MocksType = FormMoneyPresenterType
    
     typealias Stubbing = __StubbingProxy_FormMoneyPresenterType
     typealias Verification = __VerificationProxy_FormMoneyPresenterType

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: false)

    
    private var __defaultImplStub: FormMoneyPresenterType?

     func enableDefaultImplementation(_ stub: FormMoneyPresenterType) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    
    
    
     func wantToValidateData(information: Information)  {
        
    return cuckoo_manager.call("wantToValidateData(information: Information)",
            parameters: (information),
            escapingParameters: (information),
            superclassCall:
                
                Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                ,
            defaultCall: __defaultImplStub!.wantToValidateData(information: information))
        
    }
    

	 struct __StubbingProxy_FormMoneyPresenterType: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func wantToValidateData<M1: Cuckoo.Matchable>(information: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(Information)> where M1.MatchedType == Information {
	        let matchers: [Cuckoo.ParameterMatcher<(Information)>] = [wrap(matchable: information) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyPresenterType.self, method: "wantToValidateData(information: Information)", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_FormMoneyPresenterType: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func wantToValidateData<M1: Cuckoo.Matchable>(information: M1) -> Cuckoo.__DoNotUse<(Information), Void> where M1.MatchedType == Information {
	        let matchers: [Cuckoo.ParameterMatcher<(Information)>] = [wrap(matchable: information) { $0 }]
	        return cuckoo_manager.verify("wantToValidateData(information: Information)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class FormMoneyPresenterTypeStub: FormMoneyPresenterType {
    

    

    
     func wantToValidateData(information: Information)   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: Scenes/FormMoney/FormMoneyViewController.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import UIKit


 class MockFormMoneyViewController: FormMoneyViewController, Cuckoo.ClassMock {
    
     typealias MocksType = FormMoneyViewController
    
     typealias Stubbing = __StubbingProxy_FormMoneyViewController
     typealias Verification = __VerificationProxy_FormMoneyViewController

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: FormMoneyViewController?

     func enableDefaultImplementation(_ stub: FormMoneyViewController) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    
    
    
     override var containerView: FormMoneyView {
        get {
            return cuckoo_manager.getter("containerView",
                superclassCall:
                    
                    super.containerView
                    ,
                defaultCall: __defaultImplStub!.containerView)
        }
        
        set {
            cuckoo_manager.setter("containerView",
                value: newValue,
                superclassCall:
                    
                    super.containerView = newValue
                    ,
                defaultCall: __defaultImplStub!.containerView = newValue)
        }
        
    }
    
    
    
     override var delegate: FormMoneyViewControllerDelegate? {
        get {
            return cuckoo_manager.getter("delegate",
                superclassCall:
                    
                    super.delegate
                    ,
                defaultCall: __defaultImplStub!.delegate)
        }
        
        set {
            cuckoo_manager.setter("delegate",
                value: newValue,
                superclassCall:
                    
                    super.delegate = newValue
                    ,
                defaultCall: __defaultImplStub!.delegate = newValue)
        }
        
    }
    

    

    
    
    
     override func loadView()  {
        
    return cuckoo_manager.call("loadView()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.loadView()
                ,
            defaultCall: __defaultImplStub!.loadView())
        
    }
    
    
    
     override func viewDidLoad()  {
        
    return cuckoo_manager.call("viewDidLoad()",
            parameters: (),
            escapingParameters: (),
            superclassCall:
                
                super.viewDidLoad()
                ,
            defaultCall: __defaultImplStub!.viewDidLoad())
        
    }
    

	 struct __StubbingProxy_FormMoneyViewController: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    var containerView: Cuckoo.ClassToBeStubbedProperty<MockFormMoneyViewController, FormMoneyView> {
	        return .init(manager: cuckoo_manager, name: "containerView")
	    }
	    
	    
	    var delegate: Cuckoo.ClassToBeStubbedOptionalProperty<MockFormMoneyViewController, FormMoneyViewControllerDelegate> {
	        return .init(manager: cuckoo_manager, name: "delegate")
	    }
	    
	    
	    func loadView() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyViewController.self, method: "loadView()", parameterMatchers: matchers))
	    }
	    
	    func viewDidLoad() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockFormMoneyViewController.self, method: "viewDidLoad()", parameterMatchers: matchers))
	    }
	    
	}

	 struct __VerificationProxy_FormMoneyViewController: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    
	    var containerView: Cuckoo.VerifyProperty<FormMoneyView> {
	        return .init(manager: cuckoo_manager, name: "containerView", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    
	    var delegate: Cuckoo.VerifyOptionalProperty<FormMoneyViewControllerDelegate> {
	        return .init(manager: cuckoo_manager, name: "delegate", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func loadView() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("loadView()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func viewDidLoad() -> Cuckoo.__DoNotUse<(), Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("viewDidLoad()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}
}

 class FormMoneyViewControllerStub: FormMoneyViewController {
    
    
     override var containerView: FormMoneyView {
        get {
            return DefaultValueRegistry.defaultValue(for: (FormMoneyView).self)
        }
        
        set { }
        
    }
    
    
     override var delegate: FormMoneyViewControllerDelegate? {
        get {
            return DefaultValueRegistry.defaultValue(for: (FormMoneyViewControllerDelegate?).self)
        }
        
        set { }
        
    }
    

    

    
     override func loadView()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
     override func viewDidLoad()   {
        return DefaultValueRegistry.defaultValue(for: (Void).self)
    }
    
}


// MARK: - Mocks generated from file: Services/DetailService.swift at 2021-03-03 22:30:54 +0000


import Cuckoo
@testable import Teste_iOS

import UIKit


 class MockDetailService: DetailService, Cuckoo.ClassMock {
    
     typealias MocksType = DetailService
    
     typealias Stubbing = __StubbingProxy_DetailService
     typealias Verification = __VerificationProxy_DetailService

     let cuckoo_manager = Cuckoo.MockManager.preconfiguredManager ?? Cuckoo.MockManager(hasParent: true)

    
    private var __defaultImplStub: DetailService?

     func enableDefaultImplementation(_ stub: DetailService) {
        __defaultImplStub = stub
        cuckoo_manager.enableDefaultStubImplementation()
    }
    

    

    

    

	 struct __StubbingProxy_DetailService: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	     init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	}

	 struct __VerificationProxy_DetailService: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	     init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	}
}

 class DetailServiceStub: DetailService {
    

    

    
}
