import Foundation
struct DetailViewModel {
    var result: String
    var appliedValue: String
    var bruteValue: String
    var rendimentValue: String
    var taxes: String
    var liquidValue: String
    var rescueDay: String
    var daysPassed: String
    var monthlyRendiment: String
    var cdiPercentual: String
    var rentabilityYear: String
    var rentabilityPeriod: String
}
