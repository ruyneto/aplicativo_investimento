struct DetailFactory {
    static func makeDetailViewController(information: CorrectInformation) -> DetailViewController {
        let presenter = DetailPresenter(information: information)
        let viewController = DetailViewController(presenter: presenter)
        presenter.viewController = viewController
        return viewController
    }
}
