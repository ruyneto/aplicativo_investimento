struct FormMoneyFactory {
    static func createFormMoneyViewController() -> FormMoneyViewController {
        let presenter = FormMoneyPresenter()
        let viewController = FormMoneyViewController( presenter: presenter)
        presenter.viewController = viewController
        return viewController
    }
}
