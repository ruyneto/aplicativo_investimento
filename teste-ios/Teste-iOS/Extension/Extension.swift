import Foundation
extension String {
    func formatToMoney() -> String {
        let word = self
        let onlyNumbers = word.filter("0123456789".contains)
        guard let decimalNumber = Decimal(string: onlyNumbers) else {return ""}
        let comaNumber = decimalNumber/100.0
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.numberStyle = .currency
        formatter.usesGroupingSeparator = true
        guard let doubleNumber = Double("\(comaNumber)") as NSNumber?,
              let finalNumber = formatter.string(from: doubleNumber) else {return ""}
        return "\(finalNumber)"
    }

    func formatDate() -> String {
        let word = self
        var onlyNumbers = word.filter("0123456789".contains)
        if onlyNumbers.count>8 {
            onlyNumbers.removeFirst()
        }
        let zeroFill = String(repeating: "0", count: 8 - onlyNumbers.count) + onlyNumbers
        let year = "\(zeroFill.dropFirst(4))"
        let day  = "\(zeroFill.dropLast(6))"
        let rangeMinimum = Index(utf16Offset: 2, in: zeroFill)
        let rangeMaximum = Index(utf16Offset: 4, in: zeroFill)
        return " \(day)/\(zeroFill[rangeMinimum..<rangeMaximum])/\(year)"
    }

    func formatPercentage() -> String {
        let word = self
        var onlyNumbers = word.filter("0123456789".contains)
        if let auxNumber = Int(onlyNumbers) {
            let intValue = "\(Int(auxNumber))"
            onlyNumbers = intValue
        }
        if onlyNumbers.count < 1 {
            onlyNumbers.insert("0", at: onlyNumbers.index(onlyNumbers.startIndex, offsetBy: 0))
        }
        return onlyNumbers+"%"
    }

    func formatToBrazilian() -> String {
        let word = self
        let formatter1 = DateFormatter()
        formatter1.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss"
        let formatter2 = DateFormatter()
        formatter2.dateFormat =  "dd/MM/yyyy"
        guard let date = formatter1.date(from: word) else {return ""}
        let dateToBrazilian = formatter2.string(from: date)
        return dateToBrazilian
    }
}

extension Decimal {
    func formatPercentageResponse() -> String {
        let word = "\(self)"
        let splitted = word.split(separator: ".")
        splitted.forEach({$0.replacingOccurrences(of: ",", with: ".")})
        let together = splitted.joined(separator: ",")
        return together+"%"
    }
}

extension Double {
    func formatFloatNumber() -> String {
        let value: Double = (self*10)/10
        let valuex = String(format: "%.2f", value)
        return valuex
    }

    func formatToMoney() -> String {
        let value: Double = (self*10)/10
        let valuex = String(format: "R$ %.2f", locale: Locale(identifier: "pt_BR"), value)
        return valuex
    }
}
